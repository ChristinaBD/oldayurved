<?php
/**
 * Template part for thank-you
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 *
 * @package WooFramework
 * @subpackage Template
 */

/**
 * Settings for this template file.
 *
 * This is where the specify the HTML tags for the title.
 * These options can be filtered via a child theme.
 *
 * @link http://codex.wordpress.org/Plugin_API#Filters
 */
?>

<section id="section-thank-you">
<div class="header-stripe">
   <div class="header-logo">
       <img src="<?php echo get_theme_file_uri('./includes/images/logo.png') ?>" alt="">  
   </div>
</div>
<div class="header-background headerThankYou-background">
   <div class="container">
      <div class="row">
            <div class="col-lg-6 header-content">
               <h2>Takk for bestillingen!</h2>
               <p>Vi har sendt deg en e-postbekreftelse med bestillingsbekreftelsen. Hvis du ikke får e-posten i løpet av noen 
                  minutter, bør du først sjekke at e-posten du har oppgitt (som står ovenfor) er riktig, og deretter sjekke at den 
                  ikke har ikke blitt tatt av spam-filteret ditt, det skjer av og til. Hvis du har spørsmål angående bestillingen din, 
                  kan du ringe oss på 55 13 69 00 eller sende oss en e-post til post@ayurved.no.</p>
               
            </div>

            <div class="col-lg-6 header-img">
               <img src="<?php echo get_theme_file_uri('./includes/images/peace_of_mind.png') ?>" alt=""> 
            </div>

      </div>
      <div class="header-btn">
                  <button class="btn-to-form">Gjør en ny ordre</button>
            </div>
   </div>
</div>
</section>


