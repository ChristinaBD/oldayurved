<?php
/**
 * Template part for campaing
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 *
 * @package WooFramework
 * @subpackage Template
 */

/**
 * Settings for this template file.
 *
 * This is where the specify the HTML tags for the title.
 * These options can be filtered via a child theme.
 *
 * @link http://codex.wordpress.org/Plugin_API#Filters
 */
?>


<section>
<div class="header-stripe">
   <div class="header-logo">
       <img src="<?php echo get_theme_file_uri('./includes/images/logo.png') ?>" alt="">  
   </div>
</div>
<div class="header-background">
   <div class="container">
      <div class="row">
            <div class="col-lg-6 header-content">
               <h2>PEACE OF MIND</h2>
               <p>Peace of Mind inneholder Brahmi (Bacopa monnieri) med dokumenterte 
                  effekter på mange mentale funksjoner, blant annet:</p>
               <div class="header-list">
                     <div class="header-list-item">
                        <img src="<?php echo get_theme_file_uri('./includes/images/Group.png') ?>" alt="">
                        <p>Lett beroligende og stressreduserende</p>
                     </div>

                     <div class="header-list-item">
                        <img src="<?php echo get_theme_file_uri('./includes/images/Group.png') ?>" alt="">
                        <p>Bedrer konsentrasjon og fokus hos eldre</p>
                     </div>

                     <div class="header-list-item">
                        <img src="<?php echo get_theme_file_uri('./includes/images/Group.png') ?>" alt="">
                        <p>Bedrer eksamensresultater hos studenter</p>
                     </div>
               </div>
            </div>

            <div class="col-lg-6 header-img">
               <img src="<?php echo get_theme_file_uri('./includes/images/peace_of_mind.png') ?>" alt=""> 
            </div>

      </div>
      <div class="header-btn">
                  <button class="btn-to-form">Kjøp Peace of Mind nå!</button>
            </div>
   </div>
</div>
</section>

<section class="campaing-conditions">
   <div class="container">
      <div class="row">

         <div class="col-lg-4 col-12">
            <div class="conditions-box">
               <img  src="<?php echo get_theme_file_uri('./includes/images/hand.svg') ?>" alt="">
               <p>-50% på første forsendelse</p>
            </div>
         </div>

         <div class="col-lg-4 col-12">
         <div class="conditions-box">
            <img  src="<?php echo get_theme_file_uri('./includes/images/car.svg') ?>" alt="">
            <p>Rask levering - Fri Frakt</p>
         </div>
         </div>

         <div class="col-lg-4 col-12">
         <div class="conditions-box">
            <img src="<?php echo get_theme_file_uri('./includes/images/edit.svg') ?>" alt="">
            <p>Ingen bindingstid</p>
         </div>
         </div>

      </div>
   </div>
</section>


<section>
   <div class="reviews-background">
      <div class="container">
         <div class="review-headerTitle">Noen kundeuttalelser</div>
         <div class="row">
            <div class="col-4">
               <img class="review-avatar" src="<?php echo get_theme_file_uri('./includes/images/review.png') ?>" alt="">
               <div class="review-title">Peace of Mind gav meg indre ro</div>
               <div class="review-text">“Etter å ha tatt Peace of Mind i noen uker merket jeg at jeg fikk en god indre ro og at stresset slapp taket.”</div>
               <div class="review-author">Bodil Lier</div>
            </div>
            <div class="col-4">
               <img class="review-avatar" src="<?php echo get_theme_file_uri('./includes/images/reviewSecond.png') ?>" alt="">
               <div class="review-title">Bedre konsentrasjon</div>
               <div class="review-text">“I en travel hverdag kan utfordringene komme tett.  Når jeg tar Peace of Mind over tid merker jeg at fokus og hukommelse bedres.”</div>
               <div class="review-author">Tore Mjøs</div>
            </div>
            <div class="col-4">
               <img style="height:167px;" class="review-avatar" src="<?php echo get_theme_file_uri('./includes/images/reviewThird.png') ?>" alt="">
               <div class="review-title">Bedre søvn</div>
               <div class="review-text">“Etter en veldig stressende periode har Peace of Mind hjulpet meg til å sove bedre, og bli kvitt spenninger i nakken.”</div>
               <div class="review-author">Wenche Strand</div>
            </div>
         </div>
      </div>
   </div>
</section>
<section>
   <div class="benefit-title">Fordeler med Peace of Mind</div>
   
   <div class="container">
      <div class="row" id="benefit-rowPadding">
         <div class="col">
            <div class="benefit-tableTitle">
               <img id="benefit-img" src="<?php echo get_theme_file_uri('./includes/images/Group.png') ?>" alt="">
               <div class="benefit-tableTitleText">Motvirker stress</div>
            </div>
            <div class="benefit-tableText">
               Peace of Mind er et lett beroligende naturmiddel fra Ayurveda, verdens eldste helsevitenskap, som bedrer din evne til å takle stress. Urtene Bacopa monnieri, Alpinia galanga og muskatnøtt virker lett beroligende og motvirker stress, amla og sandeltre er lett avkjølende på temperamentet mens korall og perle inneholder stoffer som er bra for nervesystemet.
            </div>
         </div>
         <div class="col">
            <div class="benefit-tableTitle">
                  <img id="benefit-img" src="<?php echo get_theme_file_uri('./includes/images/Group.png') ?>" alt="">
                  <div class="benefit-tableTitleText">Synnergi effekt</div>
               </div>
               <div class="benefit-tableText">
               Peace of Mind inneholder 8 ingredienser som jobber sammen for å motvirke forskjellig aspekter av stress. Noen ingredienser er lett beroligende, andre motvirker irriatbilitet mens andre har en nærende effekt på hjerne og nervesystem. Sammen gir disse ingrediensene en helhetlig effekt for kropp og sinn.                </div>
         </div>
      </div>
      
      <div class="row" id="benefit-rowPadding">
         <div class="col">
            <div class="benefit-tableTitle">
               <img id="benefit-img" src="<?php echo get_theme_file_uri('./includes/images/Group.png') ?>" alt="">
               <div class="benefit-tableTitleText">Motvirker lett uro</div>
            </div>
            <div class="benefit-tableText">
               Peace of Mind inneholder urter som har vært brukt i årtusener for å gi mental styrke og indre ro. Dette gjør den uten å sløve, man blir faktisk mer årvåken av urtene! Bacopa monnieri, som er hovedurten i Peace of Mind, har i flere kliniske studier vist å ha evnen til å redusere stress ved å øke nivået av serotonin, et signalstoff i hjernen som bidrar til lett avslapping. Herasaponin, som også finnes i Bacopa, virker lett beroligende og kan redusere lett søvnbesvær.
            </div>
         </div>
         <div class="col">
            <div class="benefit-tableTitle">
                  <img id="benefit-img" src="<?php echo get_theme_file_uri('./includes/images/Group.png') ?>" alt="">
                  <div class="benefit-tableTitleText">Styrker hukommelse og konsentrasjon</div>
               </div>
               <div class="benefit-tableText">
                  Stress gjør at konsentrasjonsevnen blir nedsatt. Peace of Mind bedrer evnen til å holde på kunnskap. Studier viser bedre eksamensresultater hos studenter som spiste Bacopa monnieri over tid, sammenlignet med kontrollgruppe. Studier viser at Bacopa, hovedingrediensen i Peace of Mind, bedrer læreevne og hukommelse hos friske eldre. Se PubMed.gov og søk på Bacopa monnieri for detaljer.
               </div>
         </div>
      </div>
   
      <div class="row" id="benefit-rowPadding">
         <div class="col">
            <div class="benefit-tableTitle">
               <img id="benefit-img" src="<?php echo get_theme_file_uri('./includes/images/Group.png') ?>" alt="">
               <div class="benefit-tableTitleText">God mot hyperaktiv adferd hos barn</div>
            </div>
            <div class="benefit-tableText">
               Man har i nyere forskning publisert på PubMed.gov vist at Bacopa reduserer rastløshet hos barn med hyperaktiv adferd i opp til 93% av studiedeltakerne, bedrer selvkontroll i opp til 89% av deltakerne og gir forbedret konsentrasjon hos 85% av deltakerne.
            </div>
         </div>
         <div class="col">
            <div class="benefit-tableTitle">
               <img id="benefit-img" src="<?php echo get_theme_file_uri('./includes/images/Group.png') ?>" alt="">
               <div class="benefit-tableTitleText">Motvirker lettere søvnbesvær</div>
            </div>
            <div class="benefit-tableText">
               Peace of Mind er en gammel oppskrift fra Ayurveda, verdens eldste helsevitenskap. I Ayurveda har man i årtusener brukt flere av urtene i Peace of Mind som Bacopa monnieri og muskattnøtt for å motvirke lettere søvnbesvær og innsovningsvansker.
            </div>
         </div>
      </div>
      <div class="benefit-epilogue">
         Nå kan du abonnere på Peace of Mind og få de første 2 månedene for kun kr 327,-, fullpris er kr 654,- så du får 50% rabatt (tilbudet gjelder kun for nye abonnenter. Kun en pakke pr husstand). Deretter får du 20% rabatt så lenge du abonnerer. Vi sender deg 2 Peace of Mind fraktfritt annenhver måned. Det er ingen bindingstid på abonnementet.
      </div>
   </div>
   
</section>


<section сlass="campaign-offer">
   <div class="container">
   <div class="row offer-container">
      <div class="col-lg-4 col-12 offer-box">
         <div>
            <img src="<?php echo get_theme_file_uri('./includes/images/box.png') ?>" alt="">
            <p>Første forsendelse (2 pakker)</p>
            <p class="offer-cost">Kr 327,00</p>
            <p>Fullpris kr 654 - 50% rabatt<br> Gratis frakt</p>
         </div>
      </div>
      <div class="col-lg-1 col-12 offer-arrow">
         <img src="<?php echo get_theme_file_uri('./includes/images/arrow-right.png') ?>" alt="">
      </div>
      <div class="col-lg-4 col-12 offer-box">
         <div>
            <img src="<?php echo get_theme_file_uri('./includes/images/box.png') ?>" alt="">
            <p>Påfølgende forsendelser - 2 pakker hver 8. uke</p>
            <p class="offer-cost">Kr 523,20</p>
            <p>Fullpris kr 654 - 20% rabatt<br> Gratis frakt</p>
         </div>   
      </div>
      <div class="col-lg-1 col-12 offer-arrow">
         <img src="<?php echo get_theme_file_uri('./includes/images/arrow-right.png') ?>" alt="">
      </div>
      <div class="col-lg-2 col-12 offer-btn">
         <button class="btn-to-form"> <h4>Abonnér</h4>ingen bindingstid</button>
      </div>
   </div>
   </div>
</section>


<section сlass="campaing-separator">
   <div class="separator-img">
   </div>
</section>

<section class="campaing-table"> 
   <div class="container">
      <div class="table-title">Produktdetaljer</div>
            <table class="table table-ingredients table-bordered">
         <tbody>
            <tr>
               <th scope="row">Aktive ingredienser</th>
               <td class="active-ingredients">Anbefalt døgndose (2 tabletter) gir et tilskudd av følgende aktive ingredienser:
                  <div>Bacopa monnieri (L.): <span>568 mg<span></div>
                  <div>Stor galanga (Alpinia galanga, Willd.): <span>377 mg<span></div>
                  <div>Lakrisrot (Glycyrrhiza glabra, L.): <span>377 mg<span></div>
                  <div>Amla (Emblica officinalis, Gaertn.): <span>188 mg<span></div>
                  <div>Sandeltre (Santalum album, L.): <span>94 mg<span></div>
                  <div>Muskat (Myristica fragrans, Houtt.): <span>94 mg<span></div>
                  <div>Koral (Corallium rubrum): <span>57 mg<span></div>
                  <div>Perle: <span>38 mg<span></div>
               </td>

            </tr>
            <tr>
               <th scope="row">Andre ingredienser</th>
               <td>Fyllemiddel (maisstivelse, akasiegummi), overflatebehandlingsmiddel (talkum, magnesiumsalter og fettsyrer)</td>
            </tr>
            <tr>
               <th scope="row">Produkt type</th>
               <td>Urtetablett</td>

            </tr>
            <tr>
               <th scope="row">Inneholder</th>
               <td colspan="2">60 tabletter - 60g</td>
            </tr>
            <tr>
               <th scope="row">Produsent</th>
               <td colspan="2">Maharishi Ayurveda Products</td>
            </tr>
         </tbody>
         </table>

   </div>
</section>


<section class="campaing-form" id="campaing_form">
   <div class="container">
   <div class="form-title">Bestillingsskjema</div>
      <?php the_content(); ?>
   </div>
</section>