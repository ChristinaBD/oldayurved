$(document).ready(function() {
    $('.page-template-template-campaing .btn-to-form').on('click', function() {
        var dest = "#campaing_form";
        if (dest !== undefined && dest !== '') {
            $('html').animate({
                scrollTop: $(dest).offset().top
            }, 500);
        }
        return false;
    });
    $('.page-template-template-thank-you .site-footer').addClass('footer-thankYou');
    $('.page-template-template-thank-you .btn-to-form').on('click', () => {
        window.location.href = 'https://ayurved.no/';
    });
});