<?php
/**
 * Footer Template
 *
 * Here we setup all logic and XHTML that is required for the footer section of all screens.
 *
 * @package WooFramework
 * @subpackage Template
 */

 global $woo_options;

 woo_footer_top();
 	woo_footer_before();
?>
<?php if (is_page_template('template-campaing.php') || is_page_template('template-thank-you.php') ) :?>
</div><!-- #content -->
	<footer class="site-footer" id="footer-campaing">
		<div class="container">
			<div class="row ">
				<div class="col-lg-3 col-12" id="about-us">
					<h4>Om oss</h4>
					<div class="about-usText">
						Dette nettsteded og ayurved.no drives av:
						<br>
						Maharishi Ayur-Ved Produkter AS,<br>
						Postboks 200 Rådal,<br>
						5858 Bergen <br>
						Tlf: 55136900 <br>
						Org. no: 861067742
					</div>
				</div>

				<div class="col-lg-3 col-12" id="about-company">
					<a href="https://ayurved.no/kjopsinfo-og-betingelser"><h4>Kjøpsbetingelser</h4></a>
				</div>

				<div class="col-lg-3 col-12" id="purchase-terms">
				<a href="https://ayurved.no/personvern"><h4>Personvern</h4></a>
				</div>

				<div class="col-lg-3 col-12" id="privacy-andContact">				
					<a href="https://ayurved.no/contacts"><h4>Kontakt oss</h4></a>
				</div>

			</div>
		</div>
	</footer>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>

<?php 

else :	?>
	<footer id="footer" class="col-full">

		<?php woo_footer_inside(); ?>

		<div id="copyright" class="col-left">
			<?php woo_footer_left(); ?>
		</div>

		<div id="credit" class="col-right">
			<?php woo_footer_right(); ?>
		</div>

	</footer>

	<?php woo_footer_after(); ?>

	</div><!-- /#inner-wrapper -->

</div><!-- /#wrapper -->

<div class="fix"></div><!--/.fix-->

<?php wp_footer(); ?>
<?php woo_foot(); ?>
</body>
</html>

<?php endif; ?>