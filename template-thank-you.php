<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) exit;
/**
 * Template Name: thank You Template
 *
 * Handles to show thank-you page content
 *
 * @package WooFramework
 * @subpackage Template
 **/

get_header(); ?>​
        <div id="primary" class="content-area one-column content-thank-you">
            <div id="content" class="site-content">
                <?php // Start the loop.
                while ( have_posts() ) : the_post();
                    // Include the getting there page content template.
                    get_template_part( 'content', 'thank-you' );
                endwhile; ?>
            </div><!--/#content-->
        </div><!--/#primary-->
<?php get_footer(); ?>

